export default {
  tags: {
    permission: 'tag.tags.manage',
    activated: true,//
    path: '/qtag/tags/index',
    name: 'qtag.admin.tags.index',//
    page: () => import('@imagina/qtag/_layouts/admin/tags/index'),
    layout: () => import('@imagina/qsite/_layouts/master.vue'),
    title: 'qtag.sidebar.admin.tags',
    icon: 'fas fa-store',
    authenticated: true
  },
}
