const pages = config('pages') // Get Pages from config

//Marketplace
export default [
   {
      title: 'qtag.sidebar.admin.menu.manageTags',
      icon: 'fas fa-map-marked-alt',
      children: [
        pages.qtag.tags
      ]
   }
]
