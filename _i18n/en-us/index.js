import layout from '@imagina/qtag/_i18n/en-us/layout'
import sidebar from '@imagina/qtag/_i18n/en-us/sidebar'

export default {
  layout,
  sidebar
}
